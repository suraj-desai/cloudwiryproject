import React, { useState } from 'react';
import './App.css';
import PlanType from './components/PlanType';
import PlanTerm from './components/PlanTerm';
import PaymentOption from './components/PaymentOption';
import Account from './components/Account';
import CheckboxLabels from './components/CheckboxLabels';
import Graph from './components/Graph';
import Filters from './components/Filters'
import GraphInfo from './components/GraphInfoComponents/GraphInfo';

function App() {

  const initialValues={
    highSaving:30.74,
    estiAmount:5028.64,
    expValue:0.99,
    recAmount:37.5,
    days:7,
  }
  const [values,setValues]=useState(initialValues);
  const initialBtnState = {
    firstBtn:"default",
    secondBtn:"default",
    thirdBtn:"dafault",
  };

  const [btn,setBtn]=useState(initialBtnState);
  const updateFieldBtn1 = e => {
    setBtn(
      { ...initialBtnState,
        [e.target.name]:"primary",
      }
    );
    setValues({
      ...values,
      days:3,
    })
  };

  const updateFieldBtn2 = e => {
    setBtn(
      { ...initialBtnState,
        [e.target.name]:"primary",
      }
    );
    setValues({
      ...values,
      days:7,
    })
    
  };
  const updateFieldBtn3 = e => {
    setBtn(
      { ...initialBtnState,
        [e.target.name]:"primary",
      }
    );
    setValues({
      ...values,
      days:1,
    })
    
  };
  return (
    <div className="app">
      <section className="plan">
        <p>
          Savings Plan Parameters
        </p>
        <div className="plan__parameters">
            <PlanType/>
            <PlanTerm/>
            <PaymentOption/>
            <div className="purchase">
              <label>Purchase in Account</label>
              <Account/>
              <CheckboxLabels/>
            </div>
        </div>
      </section>
      <section className="graph">
        <div className="graphLeft">
          <p>
            Monthly Savings Vs Hourly Commitment
          </p>
          <div className="lineGraph">
            <Graph/>
            <div className="filters">
              <p>
                Filters by recent days:
              </p>
              <div className="filterDays">
               <Filters 
               updateFieldBtn1={updateFieldBtn1}
               updateFieldBtn2={updateFieldBtn2}
               updateFieldBtn3={updateFieldBtn3}
               btn={btn}
               />
              </div>
            </div>
          </div>
        </div>
        <div className="graphInfo">
          <GraphInfo
           highSaving={values.highSaving}
           recAmount={values.recAmount} 
           expValue={values.expValue} 
           estiAmount={values.estiAmount}
           days={values.days}
           />
        </div>
      </section>
      
    </div>
  );
}

export default App;

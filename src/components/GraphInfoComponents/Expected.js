import React from 'react';
import AssessmentIcon from '@material-ui/icons/Assessment';
export default function Expected(props){
    return(
        <div className="expected">
            Expected Target Coverage<br/> 
            {props.expValue}%
            <div className="temp">
                <div><AssessmentIcon/></div>
                <div class="expectedView">View Your Current Coverage</div>
            </div>
        </div>
    )
}
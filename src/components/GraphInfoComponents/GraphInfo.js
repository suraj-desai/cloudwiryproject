import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Recommend from './Recommend';
import Estimated from './Estimated';
import Expected from './Expected';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    margin: theme.spacing(0),
  },
  withoutLabel: {
    marginTop: theme.spacing(3),
  },
  textField: {
    width: '25ch',
  },
}));

export default function InputAdornments(props) {
  const classes = useStyles();
  const initialHighSaving={
    highSaving: props.highSaving,
  }
  const [values, setValues] = React.useState(initialHighSaving);

  const handleChange =(event) => {
    setValues({ ...values,highSaving: event.target.value });
  };

  return (
      <div>
        <div>
           Hourly Commitment:
        </div>
        <div className={classes.root}>
            <div>
                <FormControl  className={classes.margin}>
                <Input
                    id="standard-adornment-amount"
                    value={values.highSaving}
                    onChange={handleChange}
                    startAdornment={<InputAdornment position="start">$</InputAdornment>}
                />
                <FormHelperText id="my-helper-text">Default value is highest savings from graph.</FormHelperText>
                </FormControl>
            </div>
        </div>
        <Recommend recAmount={props.recAmount} days={props.days}/>
        <Expected expValue={props.expValue}/>
        <Estimated estiAmount={props.estiAmount}/>
      </div>
  );
}





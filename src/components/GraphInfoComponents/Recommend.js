import React from 'react';
export default function Recommend(props){
    let period;
    if(props.days===1){
        period=props.days+"-"+"Months";
    }
    else{
        period=props.days+"-"+"Days";
    }
    return (
        <div className="recommend">
            AWS Recommendation: ${props.recAmount}
                <div className="recommend helper">
                {period} analysis by AWS
                 </div>
        </div>
    )
}
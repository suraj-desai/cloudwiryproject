import React from 'react'

export default function Estimated(props){
    return (
        <div className="estimated">
            Estimated Net Additional Monthly Savings<br/>
            ${props.estiAmount}
            <div className="estimated helper">
                Estimated savings produced by savings plan purchase
            </div>
        </div>
    )
}
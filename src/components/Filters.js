import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(0),
    },
  },
}));

export default function Filters(props){
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Button
       variant="outlined"
       name="firstBtn" 
       color={props.btn.firstBtn}
       onClick={props.updateFieldBtn1}>
       3-Days 
      </Button>
      <Button 
        variant="outlined"
        name="secondBtn"
        color={props.btn.secondBtn} 
        onClick={props.updateFieldBtn2}>
        7-Days
      </Button>
      <Button 
        variant="outlined" 
        name="thirdBtn" 
        color={props.btn.thirdBtn} 
        onClick={props.updateFieldBtn3} >
        1-Months
      </Button>
      
    </div>
  );
}


